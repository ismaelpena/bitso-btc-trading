package com.bitso.btc.trading.domain;

import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class TradeResponse {

    private Boolean success;
    private List<Trade> payload;
    public Boolean getSuccess() {
        return success;
    }
    public void setSuccess(Boolean success) {
        this.success = success;
    }
    public List<Trade> getPayload() {
        return payload;
    }
    public void setPayload(List<Trade> payload) {
        this.payload = payload;
    }
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
