package com.bitso.btc.trading.domain;

import java.math.BigDecimal;
import java.time.OffsetDateTime;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class Trade {

    private String book;
    private OffsetDateTime createdAt;
    private BigDecimal amount;
    private MakerSide makerSide;
    private BigDecimal price;
    private String tid;

    public MakerSide getMakerSide() {
        return makerSide;
    }
    public void setMakerSide(MakerSide makerSide) {
        this.makerSide = makerSide;
    }
    public String getBook() {
        return book;
    }
    public void setBook(String book) {
        this.book = book;
    }
    public OffsetDateTime getCreatedAt() {
        return createdAt;
    }
    public void setCreatedAt(OffsetDateTime createdAt) {
        this.createdAt = createdAt;
    }
    public BigDecimal getAmount() {
        return amount;
    }
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getPrice() {
        return price;
    }
    public void setPrice(BigDecimal price) {
        this.price = price;
    }
    public String getTid() {
        return tid;
    }
    public void setTid(String tid) {
        this.tid = tid;
    }
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
