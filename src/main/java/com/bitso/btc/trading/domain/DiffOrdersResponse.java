package com.bitso.btc.trading.domain;

import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;

//{"type":"diff-orders","book":"btc_mxn","payload":[{"o":"4Qd09VQ3msO7Js55","d":1505524430939,"r":"70445.03","t":0,"s":"cancelled"}],"sequence":27846383}
public class DiffOrdersResponse {

    private String type;
    private String book;
    private String sequence;
    private List<DiffOrder> payload;
    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }
    public String getBook() {
        return book;
    }
    public void setBook(String book) {
        this.book = book;
    }
    public String getSequence() {
        return sequence;
    }
    public void setSequence(String sequence) {
        this.sequence = sequence;
    }
    public List<DiffOrder> getPayload() {
        return payload;
    }
    public void setPayload(List<DiffOrder> payload) {
        this.payload = payload;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
