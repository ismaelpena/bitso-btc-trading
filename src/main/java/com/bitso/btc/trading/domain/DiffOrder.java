package com.bitso.btc.trading.domain;

import java.math.BigDecimal;
import java.time.Instant;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class DiffOrder {

    private String o;
    private Instant d;
    private BigDecimal r;
    private Integer t;
    private BigDecimal a;
    private BigDecimal v;
    private String s;
    public String getO() {
        return o;
    }
    public void setO(String o) {
        this.o = o;
    }
    public Instant getD() {
        return d;
    }
    public void setD(Instant d) {
        this.d = d;
    }
    public BigDecimal getR() {
        return r;
    }
    public void setR(BigDecimal r) {
        this.r = r;
    }
    public Integer getT() {
        return t;
    }
    public void setT(Integer t) {
        this.t = t;
    }
    public BigDecimal getA() {
        return a;
    }
    public void setA(BigDecimal a) {
        this.a = a;
    }
    public BigDecimal getV() {
        return v;
    }
    public void setV(BigDecimal v) {
        this.v = v;
    }
    public String getS() {
        return s;
    }
    public void setS(String s) {
        this.s = s;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
