package com.bitso.btc.trading.domain;

import java.time.OffsetDateTime;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class OrderBookPayload {

    private OffsetDateTime updatedAt;
    private String sequece;
    private List<Ask> asks;
    private List<Bid> bids;
    public OffsetDateTime getUpdatedAt() {
        return updatedAt;
    }
    public void setUpdatedAt(OffsetDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }
    public String getSequece() {
        return sequece;
    }
    public void setSequece(String sequece) {
        this.sequece = sequece;
    }
    public List<Ask> getAsks() {
        return asks;
    }
    public void setAsks(List<Ask> asks) {
        this.asks = asks;
    }
    public List<Bid> getBids() {
        return bids;
    }
    public void setBids(List<Bid> bids) {
        this.bids = bids;
    }
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
