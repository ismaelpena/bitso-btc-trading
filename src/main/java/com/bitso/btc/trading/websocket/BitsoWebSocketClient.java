package com.bitso.btc.trading.websocket;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.websocket.ClientEndpointConfig;
import javax.websocket.ClientEndpointConfig.Configurator;
import javax.websocket.ContainerProvider;
import javax.websocket.DeploymentException;
import javax.websocket.Extension;
import javax.websocket.Session;
import javax.websocket.WebSocketContainer;

import org.apache.tomcat.websocket.Constants;
import org.apache.tomcat.websocket.PerMessageDeflate;
import org.apache.tomcat.websocket.TransformationFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bitso.btc.trading.handler.BitsoHandler;

@Service
public class BitsoWebSocketClient {

    private static final Logger logger = LoggerFactory.getLogger(BitsoWebSocketClient.class);

    private WebSocketContainer container = ContainerProvider.getWebSocketContainer();
    
    private Session s;

    @Autowired
    private BitsoHandler bitsoHandler;

    @PostConstruct
    public void init(){
        connect();
        //subscribe();
    }

    public void connect(){
        logger.debug("connection to bitso web socket");
        List<Extension> extensions = new ArrayList<>();
        List<Extension.Parameter> list = new ArrayList<>();
        list.add(
                new Extension.Parameter(){

                    @Override
                    public String getName() {
                        return "client_max_window_bits";
                    }

                    @Override
                    public String getValue() {
                        return null;
                    }
                }
                );

        List<List<Extension.Parameter>> params = new ArrayList<>();
        params.add(list);

        extensions.add(
                TransformationFactory.getInstance().create(
                        PerMessageDeflate.NAME, 
                        params,
                        false).getExtensionResponse()
                );


        Configurator configurator = new Configurator(){
            public void beforeRequest(Map<String, List<String>> headers) {
                headers.put(Constants.HOST_HEADER_NAME, Arrays.asList("ws.bitso.com"));
            }
        };

        ClientEndpointConfig cec = ClientEndpointConfig.Builder.create().configurator(configurator).extensions(extensions).build();
        
        try {
            s = container.connectToServer(new WSClient(), cec, URI.create("wss://ws.bitso.com"));
            s.addMessageHandler(bitsoHandler);

        } catch (DeploymentException | IOException e) {
            logger.error(e.getMessage(),e);
        }
    }

    public void subscribe(){

        try {
            if(s!=null){
                s.getBasicRemote().sendText("{ \"action\": \"subscribe\", \"book\": \"btc_mxn\", \"type\": \"diff-orders\" }");
            }else{
                logger.debug("session is not initialized");
            }
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        }
    }
}
