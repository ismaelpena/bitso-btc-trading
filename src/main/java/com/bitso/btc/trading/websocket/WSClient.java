package com.bitso.btc.trading.websocket;

import javax.websocket.CloseReason;
import javax.websocket.Endpoint;
import javax.websocket.EndpointConfig;
import javax.websocket.Session;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class WSClient extends Endpoint {

    private static final Logger logger = LoggerFactory.getLogger(WSClient.class);

    @Override
    public void onOpen(Session session, EndpointConfig config) {
        logger.debug("opening session: {} with config: {}", session, config);
    }

    @Override
    public void onClose(Session session, CloseReason closeReason) {
        logger.debug("closing session: {} with reason: {}", session, closeReason);
    }

    @Override
    public void onError(Session session, Throwable throwable) {
        logger.debug("error in session: {}", session, throwable);
    }   
}