package com.bitso.btc.trading.controller;

import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

@Controller
public class TradeController {

    @MessageMapping("/trade")
    @SendTo("/topic/trade")
    public String trade(String message) throws Exception {
        //just echo
        return message;
    }

    @MessageMapping("/orderbook")
    @SendTo("/topic/orderbook")
    public String orderBook(String message) throws Exception {
        //just echo
        return message;
    }
}
