package com.bitso.btc.trading.service;

import java.util.Collections;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.bitso.btc.trading.domain.TradeResponse;

@Service
public class BitsoBtcTradeService {
    private static final Logger logger = LoggerFactory.getLogger(BitsoBtcOrderBookService.class);

    @Autowired
    private RestTemplate restTemplate;

    private static final String URL = "https://api.bitso.com/v3/trades/?book=btc_mxn";

    @Autowired
    private SimpMessagingTemplate template;

    @Value("${bitso.max-trade-result}")
    private Integer maxTradeResult;

    @PostConstruct
    public void init(){
        start();
    }

    public void start(){
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.ALL));
        headers.set(HttpHeaders.HOST, "api.bitso.com");
        headers.set(HttpHeaders.USER_AGENT, "curl/7.47.0");
        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);

        ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);

        Runnable task = () -> {
            ResponseEntity<TradeResponse> response  = restTemplate.exchange(
                    URL,
                    HttpMethod.GET,
                    entity,
                    TradeResponse.class);

            logger.debug("trade call response: {}", response.getBody());

            TradeResponse tr = new TradeResponse();
            tr.setPayload(response.getBody().getPayload());
            tr.setSuccess(response.getBody().getSuccess());
            if(response.getBody().getSuccess() && 
                    response.getBody().getPayload().size() > maxTradeResult){
                tr.setPayload(tr.getPayload().subList(0, maxTradeResult));
            }

            template.convertAndSend("/topic/trade", tr);
        };

        int initialDelay = 0;
        int period = 1;
        executor.scheduleAtFixedRate(task, initialDelay, period, TimeUnit.MINUTES);

    }

}
