package com.bitso.btc.trading.service;

import java.util.Collections;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.bitso.btc.trading.domain.OrderBookResponse;
import com.bitso.btc.trading.handler.BitsoHandler;
import com.bitso.btc.trading.websocket.BitsoWebSocketClient;

@Service
@DependsOn("bitsoWebSocketClient")
public class BitsoBtcOrderBookService {

    private static final Logger logger = LoggerFactory.getLogger(BitsoBtcOrderBookService.class);

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private BitsoWebSocketClient bitsoWebSocketClient;

    @Autowired
    private BitsoHandler bitsoHandler;

    private static final String URL = "https://api.bitso.com/v3/order_book/?book=btc_mxn&aggregate=false";

    public BitsoBtcOrderBookService(){
    }

    @PostConstruct
    public void init(){
        start();
    }

    public void start(){

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.ALL));
        headers.set(HttpHeaders.HOST, "api.bitso.com");
        headers.set(HttpHeaders.USER_AGENT, "curl/7.47.0");
        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);

        ResponseEntity<OrderBookResponse> response  = restTemplate.exchange(
                URL,
                HttpMethod.GET,
                entity,
                OrderBookResponse.class);


        logger.debug("trading response: {}", response.getBody());

        if(response.getBody() != null && 
                response.getBody().getSuccess() == true && 
                response.getBody().getPayload() != null){
            bitsoHandler.setOrderBookPayload(response.getBody().getPayload());
            bitsoWebSocketClient.subscribe();
        }

    }
}
