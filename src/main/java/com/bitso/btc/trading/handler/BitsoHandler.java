package com.bitso.btc.trading.handler;

import java.util.Optional;
import java.util.stream.Collectors;

import javax.websocket.MessageHandler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;

import com.bitso.btc.trading.domain.Ask;
import com.bitso.btc.trading.domain.DiffOrder;
import com.bitso.btc.trading.domain.DiffOrdersResponse;
import com.bitso.btc.trading.domain.Movement;
import com.bitso.btc.trading.domain.OrderBookPayload;
import com.google.gson.Gson;

@Component
public class BitsoHandler implements MessageHandler.Whole<String> {

    private static final Logger logger = LoggerFactory.getLogger(BitsoHandler.class);

    private OrderBookPayload orderBookPayload;

    @Autowired
    private SimpMessagingTemplate template;

    @Autowired
    private Gson gson;

    @Value("${bitso.max-orders-result}")
    private Integer maxOrdersResult;

    public OrderBookPayload getOrderBookPayload() {
        return orderBookPayload;
    }


    public void setOrderBookPayload(OrderBookPayload orderBookPayload) {
        this.orderBookPayload = orderBookPayload;
    }


    @Override
    public void onMessage(String message) {

        logger.trace(message);
        if(orderBookPayload == null){
            return;
        }

        DiffOrdersResponse dor = gson.fromJson(message, DiffOrdersResponse.class);
        if ( dor.getType().equals("ka") || dor.getPayload() == null) {
            return;
        }

        java.util.function.BiPredicate<Movement, DiffOrder> fx = (b,d) -> b.getOid().equals(d.getO());

        //look for a diff order into ask/bid arrays
        //i do not too much about stocks, so in order to have a good implementation here, 
        //i need to learn more about stock business rules.
        dor.getPayload().forEach( o -> {

            Optional<? extends Movement> m = orderBookPayload.getAsks().stream().filter(t -> fx.test(t,o) ).findAny();

            if(m.isPresent()){
                processMov(o, m.get());

            }else{

                m = orderBookPayload.getBids().stream().filter(t -> fx.test(t,o) ).findAny();
                if(m.isPresent()){
                    processMov(o, m.get());
                }else{
                    logger.trace("nothing to do for {} as it was not found in ask/bid arrays. Should we call order rest call again?",o);
                }
            }
        }
        );

    }

    public void processMov(DiffOrder o, Movement m){
        //actually we just know about 2 types of status
        logger.trace("coincidence for diff {} and order {}", m,o);

        switch(o.getS()){

        case "cancelled": 
            logger.debug("removing orderbook {} by msg {}", m,o);
            if(m instanceof Ask){
                orderBookPayload.getAsks().remove(m);
            }else{
                orderBookPayload.getBids().remove(m);
            }
            break;
        case "open":
            //lets assume amount->[A]mount, price->[V]alue fields changed in the order an update them.
            logger.debug("updating orderbook {} to {}", m,o);
            m.setAmount(o.getA());
            m.setPrice(o.getV());
            break;
        }
        //send a update event to UI
        sendUpdate();
    }
    public void sendUpdate(){
        //order values by max amount criteria (desc) and take top-N values (best values) 
        orderBookPayload.setAsks(
                orderBookPayload.getAsks().
                stream().
                sorted( 
                        (a, a1) -> a.getAmount().compareTo(a1.getAmount())).
                collect(Collectors.toList()));

        orderBookPayload.setBids(
                orderBookPayload.getBids().
                stream().
                sorted(
                        (a, a1) -> a.getAmount().compareTo(a1.getAmount())).
                collect(Collectors.toList()));

        //build response
        OrderBookPayload obp = new OrderBookPayload();
        obp.setAsks(orderBookPayload.getAsks());
        obp.setBids(orderBookPayload.getBids());
        obp.setSequece(orderBookPayload.getSequece());
        obp.setUpdatedAt(orderBookPayload.getUpdatedAt());

        if(orderBookPayload.getAsks().size() > maxOrdersResult ){
            obp.setAsks(obp.getAsks().subList(0,  maxOrdersResult));
        }
        if(orderBookPayload.getBids().size() > maxOrdersResult){
            obp.setBids(obp.getBids().subList(0, maxOrdersResult));
        }

        logger.debug("sending back {}", obp);
        template.convertAndSend("/topic/orderbook", obp);
        
    }
}
