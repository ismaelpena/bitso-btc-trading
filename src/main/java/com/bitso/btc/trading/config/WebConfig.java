package com.bitso.btc.trading.config;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.time.ZonedDateTime;
import java.util.Arrays;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.GsonHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import com.bitso.btc.trading.gson.adapter.BigDecimalFormatter;
import com.bitso.btc.trading.gson.adapter.InstantTypeAdapter;
import com.bitso.btc.trading.gson.adapter.LocalDateTypeAdapter;
import com.bitso.btc.trading.gson.adapter.LongTypeAdapter;
import com.bitso.btc.trading.gson.adapter.OffsetDateTimeTypeAdapter;
import com.bitso.btc.trading.gson.adapter.ZonedDateTimeTypeAdapter;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Configuration
public class WebConfig {

    @Bean
    public GsonBuilder gsonBuilder(){
        return new GsonBuilder().
                setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES).
                registerTypeAdapter(BigDecimal.class, new BigDecimalFormatter()).
                registerTypeAdapter(Long.class, new LongTypeAdapter()).
                registerTypeAdapter(Instant.class, new InstantTypeAdapter()).
                registerTypeAdapter(LocalDate.class, new LocalDateTypeAdapter()).
                registerTypeAdapter(OffsetDateTime.class, new OffsetDateTimeTypeAdapter()).
                registerTypeAdapter(ZonedDateTime.class, new ZonedDateTimeTypeAdapter()).
                serializeNulls();
    }

    @Bean
    public Gson gson(GsonBuilder gsonBuilder) {
        return gsonBuilder.create();
    }

    @Bean
    public GsonHttpMessageConverter gsonHttpMessageConverter(Gson gson) {
        GsonHttpMessageConverter gsonHttpMessageConverter = new GsonHttpMessageConverter();
        gsonHttpMessageConverter.setGson(gson);
        return gsonHttpMessageConverter;
    }
    @Bean
    public RestTemplate restTemplate(GsonHttpMessageConverter gsonHttpMessageConverter) {
        return new RestTemplate(Arrays.asList(gsonHttpMessageConverter));
    }
}
