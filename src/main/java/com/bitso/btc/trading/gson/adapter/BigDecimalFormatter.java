package com.bitso.btc.trading.gson.adapter;

import java.lang.reflect.Type;
import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

public class BigDecimalFormatter implements JsonDeserializer<BigDecimal> {

    public static final Logger logger = LoggerFactory.getLogger(BigDecimalFormatter.class);
    @Override
    public BigDecimal deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        try {
            return new BigDecimal(jsonElement.getAsString().replace(",", ""));
        } catch (NumberFormatException e) {
            logger.debug(e.getMessage());
            throw new JsonParseException(e);
        }
    }
}
