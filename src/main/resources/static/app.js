var stompClient = null;

function connect() {
    var socket = new SockJS('/bitso-btc-trading');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
        console.log('Connected: ' + frame);

        stompClient.subscribe('/topic/trade', function (response) {
        	showTrade(response);
        	
        });
        stompClient.subscribe('/topic/orderbook', function (response) {

        	showAsks(response);
        	showBids(response);
        });
    });
}

function showAsks(response){
    myRecords = JSON.parse(response.body).asks;
    
    askstable = $('#asks-table').dynatable({
	  dataset: {
	    records: myRecords
	  }
	});
    $('#asks-table').data('dynatable').dom.update();
}

function showBids(response){
    myRecords = JSON.parse(response.body).bids;
    $('#bids-table').dynatable({
	  dataset: {
	    records: myRecords
	  }
	});
    $('#bids-table').data('dynatable').dom.update();
}

function showTrade(response){
	myRecords = JSON.parse(response.body).payload;
    $('#trade-table').dynatable({
	  dataset: {
	    records: myRecords
	  }
	});
    $('#trade-table').data('dynatable').dom.update();
}

$(function () {
    connect();
});