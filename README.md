# Bitso Trading

Module to process a btc-mx trading.



----
### How to run it

Build and Run it as:

    grade build
    java -jar  build/libs/bitso-btc-trading-0.1.0.jar

### Dependencies
* gradle
* java 8
